# Command
The Command design pattern is part of the collection of design patterns documented by the "Gang of Four" (or GoF) in their now famous book, **Design Patterns:
Elements of Reusable Object-Oriented Software**.

This design pattern provides a ton of flexibility by decoupling the logic of performing an operation from the object that invokes it. Individual commands are split into classes that implement a common **ICommand** interface and references to instances of these commands are then passed to an invoker class. The users' actions are then performed on the invoker without having any knowledge of the actual commands. In addition, these commands can then be swapped in and out at runtime for added flexibility in the application.

The main problem this solves is doing away with rigid code which is inflexible. Hardcoding logic makes it almost impossible to specify a request at run-time and this pattern effectively eliminates this obstacle.


## Data Fetcher Example:
In this simple example repository we can see three ways of fetching data on varying time frames. Fetching data for the current day, the past seven days, and the past month are possible by invoking the appropriate command on the invoker class.

The commands are located in separate classes that implement the **ICommand** interface and expose the method **GetData()**. Each of these classes hold a reference to the same data source but invoke a different API call depending on which time frame is represented. Fetching data from the data source for today and for the past seven days make use of the "hot data" because it is cheap to access, however, fetching data for the past month goes to "cold storage" which has a longer retention but is more costly to access.

In the main class we simply instantiate an instance of the invoker class which is our **DataFetcher** and provide it with three commands that represent fetching data from each of the three time frames. Then when the user tries to fetch data by using one of the three commands, the action is performed and the user does not need to know about the underlying details.

All data in this example is generated with random numbers to simulate real data.


### Sample Output

Begin Fetching Data <br/>

Today's data From GoodData: <br/>
2022-06-17,74892,7807,8386,948,355,16,9,9,0,8 <br/>
<br/>
Past 7 days data From GoodData: <br/>
2022-06-16,55216,3522,7481,182,166,607,8,1,3,6 <br/>
2022-06-15,94811,3706,9284,336,344,337,2,8,0,8 <br/>
2022-06-14,22974,2019,2870,376,746,380,1,8,4,9 <br/>
2022-06-13,13172,1315,3894,651,501,110,9,5,3,3 <br/>
2022-06-12,65573,5280,4786,924,459,537,3,1,0,4 <br/>
2022-06-11,29178,5566,2324,424,665,440,0,4,3,7 <br/>
2022-06-10,46894,9625,8067,458,570,913,9,3,7,2 <br/>
<br/>
Past Month data From GoodData: <br/>
2022-05-31,48215,9986,1603,383,292,322,5,5,5,1 <br/>
2022-05-30,52323,3375,3349,541,436,354,4,4,6,8 <br/>
2022-05-29,43964,7126,1724,876,170,226,4,2,2,0 <br/>
2022-05-28,28942,9679,3876,490,233,445,1,8,7,8 <br/>
2022-05-27,43310,8807,4662,904,925,882,2,2,1,0 <br/>
2022-05-26,20371,1519,8135,519,574,798,9,2,1,0 <br/>
2022-05-25,82426,3479,4455,545,907,558,9,8,2,9 <br/>
2022-05-24,17224,6057,7442,822,464,570,2,1,9,2 <br/>
2022-05-23,93882,3408,2109,273,876,232,1,8,8,4 <br/>
2022-05-22,52655,9814,7542,703,53,160,4,9,9,3 <br/>
2022-05-21,93803,5087,9643,205,856,637,3,9,0,0 <br/>
2022-05-20,52727,7577,3398,884,136,559,1,1,6,6 <br/>
2022-05-19,38865,1818,9661,852,474,192,8,0,7,3 <br/>
2022-05-18,9202,4257,2800,610,349,489,3,0,3,2 <br/>
2022-05-17,88373,2637,8619,874,907,793,7,8,5,4 <br/>
2022-05-16,99974,3630,3624,374,988,848,4,1,6,6 <br/>
2022-05-15,80086,3666,3069,729,921,168,5,6,5,6 <br/>
2022-05-14,25515,2317,8680,441,801,208,2,1,9,6 <br/>
2022-05-13,47689,9821,5532,343,314,792,0,8,5,4 <br/>
2022-05-12,99238,2647,4217,700,447,590,9,1,9,7 <br/>
2022-05-11,26752,1119,3932,908,248,854,4,9,5,7 <br/>
2022-05-10,60618,4508,2752,972,206,820,7,9,3,1 <br/>
2022-05-09,9932,6722,2089,129,655,73,7,1,9,2 <br/>
2022-05-08,25598,1871,2460,265,424,463,6,6,0,4 <br/>
2022-05-07,35814,2420,4502,811,895,606,9,3,4,6 <br/>
2022-05-06,79554,9935,7805,828,659,628,8,2,2,1 <br/>
2022-05-05,7388,3278,9781,363,544,510,2,0,5,7 <br/>
2022-05-04,17276,2682,9366,943,780,933,2,3,1,0 <br/>
2022-05-03,8555,4610,7449,381,425,263,3,3,5,7 <br/>
2022-05-02,57041,4246,4238,867,396,107,1,0,2,3 <br/>
2022-05-01,38107,9791,8523,755,464,610,0,8,0,7 <br/>




Done Fetching Data
