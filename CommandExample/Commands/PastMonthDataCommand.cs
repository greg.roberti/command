using CommandExample.DataSources;

namespace CommandExample.Commands
{
    /// <summary>
    /// Fetch the past month of data
    /// </summary>
    public class PastMonthDataCommand : ICommand
    {
        private readonly IDataSource _dataSource;

        public PastMonthDataCommand(IDataSource dataSource)
        {
            _dataSource = dataSource;
        }

        public string GetData()
        {
            return _dataSource.FetchPastMonth();
        }
    }
}