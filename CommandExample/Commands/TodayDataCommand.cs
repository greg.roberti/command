using CommandExample.DataSources;

namespace CommandExample.Commands
{
    /// <summary>
    /// Fetch the past day of data
    /// </summary>
    public class TodayDataCommand : ICommand
    {
        private readonly IDataSource _dataSource;

        public TodayDataCommand(IDataSource dataSource)
        {
            _dataSource = dataSource;
        }

        public string GetData()
        {
            return _dataSource.FetchToday();
        }
    }
}