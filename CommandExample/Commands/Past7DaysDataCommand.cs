using CommandExample.DataSources;

namespace CommandExample.Commands
{
    /// <summary>
    /// Fetch the past 7 days of data
    /// </summary>
    public class Past7DaysDataCommand : ICommand
    {
        private readonly IDataSource _dataSource;

        public Past7DaysDataCommand(IDataSource dataSource)
        {
            _dataSource = dataSource;
        }

        public string GetData()
        {
            return _dataSource.FetchPast7Days();
        }
    }
}