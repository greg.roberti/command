namespace CommandExample.Commands
{
    /// <summary>
    /// Common interface for all Commands
    /// </summary>
    public interface ICommand
    {
        string GetData();
    }
}