using CommandExample.Commands;

namespace CommandExample
{
    /// <summary>
    /// Invoker class which will invoke the appropriate command
    /// </summary>
    public class DataFetcher
    {
        private readonly ICommand _fetchTodayDataCommand;
        private readonly ICommand _fetchPast7DaysDataCommand;
        private readonly ICommand _fetchPastMonthDataCommand;

        public DataFetcher(ICommand todayData, ICommand past7DaysData, ICommand pastMonthData)
        {
            _fetchTodayDataCommand = todayData;
            _fetchPast7DaysDataCommand = past7DaysData;
            _fetchPastMonthDataCommand = pastMonthData;
        }

        /// <summary>
        /// Fetches data from today
        /// </summary>
        public string FetchTodayData()
        {
            return _fetchTodayDataCommand.GetData();
        }

        /// <summary>
        /// Fetches data for the past 7 days
        /// </summary>
        public string FetchPast7DaysData()
        {
            return _fetchPast7DaysDataCommand.GetData();
        }

        /// <summary>
        /// Fetches data for the past month
        /// </summary>
        public string FetchPastMonthData()
        {
            return _fetchPastMonthDataCommand.GetData();
        }
    }
}