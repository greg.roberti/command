using System;
using System.Linq;
using System.Text;

namespace CommandExample.DataSources
{
    /// <summary>
    /// Fetch data from GoodData.
    /// The requests for today's data and for the past 7 days will come from the "hot data" which is readily available and cheap to access.
    /// Any request for data that is over 14 days old will need to be accessed from the "cold storage" which is slower and more expensive to access.
    /// </summary>
    public class GoodDataSource : IDataSource
    {
        /// <summary>
        /// Fetches today's data from the "hot data" which is readily available and cheap to access.
        /// </summary>
        public string FetchToday()
        {
            DateTimeOffset today = DateTimeOffset.Now.Date;
            return FetchHotData(today, today);
        }

        /// <summary>
        /// Fetches data for the past 7 days from the "hot data" which is readily available and cheap to access.
        /// </summary>
        public string FetchPast7Days()
        {
            DateTimeOffset today = DateTimeOffset.Now.Date;
            return FetchHotData(today - TimeSpan.FromDays(7), today - TimeSpan.FromDays(1));
        }

        /// <summary>
        /// Fetches data from "cold storage" which is slower and more expensive to access.
        /// </summary>
        /// <returns></returns>
        public string FetchPastMonth()
        {
            DateTimeOffset today = DateTimeOffset.Now.Date;
            return FetchColdStorageData(
                new DateTimeOffset(today.Year, today.Month - 1, 1, 0, 0, 0, today.Offset),
                new DateTimeOffset(today.Year, today.Month, 1, 0, 0, 0, today.Offset) - TimeSpan.FromDays(1)
            );
        }

        /// <summary>
        /// Requests under 14 days old will come from the "hot data"
        /// </summary>
        /// <param name="from">The start of the data</param>
        /// <param name="to">The end of the data</param>
        /// <exception cref="Exception">Requests older than 14 days will result in an exception</exception>
        private string FetchHotData(DateTimeOffset from, DateTimeOffset to)
        {
            if (DateTimeOffset.Now.Date - from >= TimeSpan.FromDays(14))
            {
                throw new Exception("Hot Data only exists for the past 2 weeks!");
            }

            var days = Enumerable.Range(0, 1 + to.Subtract(from).Days)
                .Select(offset => from.AddDays(offset)).Reverse().ToArray();

            // Generate some data
            Random rnd = new Random();
            StringBuilder sb = new StringBuilder();
            foreach (var day in days)
            {
                sb.AppendLine(day.ToString("yyyy-MM-dd") + "," +
                              rnd.Next(1000, 100000) + "," +
                              rnd.Next(1000, 10000) + "," +
                              rnd.Next(1000, 10000) + "," +
                              rnd.Next(100, 1000) + "," +
                              rnd.Next(10, 1000) + "," +
                              rnd.Next(10, 1000) + "," +
                              rnd.Next(0, 10) + "," +
                              rnd.Next(0, 10) + "," +
                              rnd.Next(0, 10) + "," +
                              rnd.Next(0, 10)
                );
            }

            return sb.ToString();
        }

        /// <summary>
        /// Requests over 14 days old will come from "cold storage"
        /// </summary>
        /// <param name="from">The start of the data</param>
        /// <param name="to">The end of the data</param>
        /// <exception cref="Exception">Requests more recent than 14 days will result in an exception</exception>
        private string FetchColdStorageData(DateTimeOffset from, DateTimeOffset to)
        {
            if (DateTimeOffset.Now.Date - from < TimeSpan.FromDays(14))
            {
                throw new Exception("Cold Data is too expensive to access for recent data, use Hot Data for accessing data from the past 2 weeks!");
            }

            var days = Enumerable.Range(0, 1 + to.Subtract(from).Days)
                .Select(offset => from.AddDays(offset)).Reverse().ToArray();

            // Generate some data
            Random rnd = new Random();
            StringBuilder sb = new StringBuilder();
            foreach (var day in days)
            {
                sb.AppendLine(day.ToString("yyyy-MM-dd") + "," +
                              rnd.Next(1000, 100000) + "," +
                              rnd.Next(1000, 10000) + "," +
                              rnd.Next(1000, 10000) + "," +
                              rnd.Next(100, 1000) + "," +
                              rnd.Next(10, 1000) + "," +
                              rnd.Next(10, 1000) + "," +
                              rnd.Next(0, 10) + "," +
                              rnd.Next(0, 10) + "," +
                              rnd.Next(0, 10) + "," +
                              rnd.Next(0, 10)
                );
            }

            return sb.ToString();
        }
    }
}