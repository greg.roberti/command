namespace CommandExample.DataSources
{
    /// <summary>
    /// Common interface for all data sources
    /// </summary>
    public interface IDataSource
    {
        string FetchToday();
        string FetchPast7Days();
        string FetchPastMonth();
    }
}