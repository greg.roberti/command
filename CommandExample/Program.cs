﻿using System;
using CommandExample.Commands;
using CommandExample.DataSources;

namespace CommandExample
{
    class Program
    {
        static void Main()
        {
            IDataSource goodDataSource = new GoodDataSource();
            DataFetcher goodDataDataFetcher = new DataFetcher(
                todayData: new TodayDataCommand(goodDataSource),
                past7DaysData: new Past7DaysDataCommand(goodDataSource),
                pastMonthData: new PastMonthDataCommand(goodDataSource)
            );

            Console.WriteLine("Begin Fetching Data");

            Console.WriteLine("\n");

            var goodDataToday = goodDataDataFetcher.FetchTodayData();
            Console.WriteLine("Today's data From GoodData: ");
            Console.WriteLine(goodDataToday);

            Console.WriteLine("\n");

            var goodDataPast7Days = goodDataDataFetcher.FetchPast7DaysData();
            Console.WriteLine("Past 7 days data From GoodData: ");
            Console.WriteLine(goodDataPast7Days);

            Console.WriteLine("\n");

            var goodDataPastMonth = goodDataDataFetcher.FetchPastMonthData();
            Console.WriteLine("Past Month data From GoodData: ");
            Console.WriteLine(goodDataPastMonth);

            Console.WriteLine("\n");

            Console.WriteLine("Done Fetching Data");
        }
    }
}